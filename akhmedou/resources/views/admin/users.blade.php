@extends('admin.dashboard')
@section('content-bis')
<div class="users-box">
    <h1>Utilisateurs</h1>
    @if(session('response'))
        <div class="success-block">
            {{ session('response') }}
        </div>
    @endif
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                  <th>Prénom NOM</th>
                  <th>Email</th>
                  <th>Date d'inscription</th>
                  <th class="status">Statut</th>
                  <th class="remove status">Supprimer</th>
                </tr>
            </thead>
        </table>
      </div>
      <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                  <td>{{ $supadmin['firstname'] }} {{ $supadmin['lastname'] }} <i class="fa fa-check" style="color:#39B54A;" aria-hidden="true"></i></td>
                  <td>{{ $supadmin['email'] }}</td>
                  <td>{{ $supadmin['updated_at'] }}</td>
                  <td class="status"><i class="fa fa-check" style="color:#39B54A;" aria-hidden="true"></i></td>
                  <td class="status"><i class="fa fa-check" style="color:#39B54A;" aria-hidden="true"></i></td>
                </tr>

                @foreach($admins as $admin)
                    <tr>
                      <td>{{ $admin['firstname'] }} {{ $admin['lastname'] }} <i class="fa fa-check" aria-hidden="true"></i></td>
                      <td>{{ $admin['email'] }}</td>
                      <td>{{ $admin['created_at'] }}</td>
                      <td class="status">
                        <label for="{{ 'update_'.$admin['id'] }}">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.users.update', $admin['id']) }}">
                          {{ csrf_field() }}
                          <input type="submit" id="{{ 'update_'.$admin['id'] }}" value="Modifier">
                        </form>
                      </td>
                      <td class="status">
                        <label for="{{ 'remove_'.$admin['id'] }}">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.users.destroy', $admin['id']) }}">
                        {{ csrf_field() }}
                        <input type="submit" id="{{ 'remove_'.$admin['id'] }}" value="Supprimer">
                        </form>
                      </td>
                    </tr>
                @endforeach

                @foreach($users as $user)
                    <tr>
                      <td>{{ $user['firstname'] }} {{ $user['lastname'] }}</td>
                      <td>{{ $user['email'] }}</td>
                      <td>{{ $user['created_at'] }}</td>
                      <td class="status">
                        <label for="{{ 'update_'.$user['id'] }}">
                          <i class="fa fa-refresh" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.users.update', $user['id']) }}">
                          {{ csrf_field() }}
                          <input type="submit" id="{{ 'update_'.$user['id'] }}" value="Modifier">
                        </form>
                      </td>
                      <td class="status">
                        <label for="{{ 'remove_'.$user['id'] }}">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.users.destroy', $user['id']) }}">
                        {{ csrf_field() }}
                        <input type="submit" id="{{ 'remove_'.$user['id'] }}" value="Supprimer">
                        </form>
                      </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
