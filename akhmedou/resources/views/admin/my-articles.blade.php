@extends('admin.dashboard')
@section('content-bis')
<div class="users-box">
    <h1>Articles</h1>
    @if(session('response'))
        <div class="success-block">
            {{ session('response') }}
        </div>
    @endif
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                  <th>Titre</th>
                  <th>Date de publication</th>
                  <th class="status">Valider</th>
                  <th class="status">Lire</th>
                  <th class="status">Editer</th>
                  <th class="remove status">Supprimer</th>
                </tr>
            </thead>
        </table>
      </div>
      <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>

                @foreach($articles as $article)
                    <tr>
                      <td>{{ $article->title }}</td>
                      <td>{{ $article->created_at->format('M j, Y') }}</td>
                      <td class="status">
                        @if($article->check == 1)
                            <i class="fa fa-check" style="color:#39B54A;" aria-hidden="true"></i>
                            @else
                                En attente

                        @endif
                      </td>
                      <td class="status">
                        @if($article->check == 1)
                            <a href="{{ route('article.read', $article->slug) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            @else
                              <span>-</span>
                        @endif
                      </td>
                      <td class="status">
                        <a href="{{ route('admin.articles.get.article.to.update', $article->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      </td>
                      <td class="status">
                        <label for="{{ 'remove_'.$article->id }}">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.articles.destroy', $article->id) }}">
                        {{ csrf_field() }}
                        <input type="submit" id="{{ 'remove_'.$article->id }}" value="Supprimer">
                        </form>
                      </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $articles->links() }}

    </div>
</div>

@endsection
