<div class="admin-sidebar">
      <ul>
          <li class="dashboard-title">
            <a href="#">Tableau de bord</a>
          </li>
          @if(Auth::user()->supadmin == 1)
              <li>
                <a href="{{ route('admin.users') }}">Utilisateurs</a>
              </li>
          @endif
          <li>
            <a href="{{ route('admin.articles.create') }}">Publier un article</a>
          </li>
          <li>
            <a href="{{ route('admin.my-articles') }}">Mes articles</a>
          </li>
          @if(Auth::user()->supadmin == 1)
            <li>
              <a href="{{ route('admin.articles') }}">Tous les articles</a>
            </li>
            <li>
              <a href="{{ route('admin.articles.to.check') }}">Articles à valider</a>
            </li>
          @endif
          <li>
            <a href="#" >Commentaires</a>
          </li>
          <li>
            <a href="{{ route('admin.articles.drafts') }}">Brouillon</a>
          </li>
      </ul>
</div>
