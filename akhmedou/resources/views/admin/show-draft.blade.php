@extends('admin.dashboard')
@section('content-bis')
<div class="articles-form-box">
    <h1>Brouillon</h1>
    @if(session('response'))
        <div class="success-block">
            {{ session('response') }}
        </div>
    @endif
    <form class="reglog-container" method="POST" action="{{ route('admin.articles.update.draft', $article->slug) }}"  enctype="multipart/form-data">
      {{ csrf_field() }}
      @if ($errors->has('title'))
          <span class="help-block">
              <strong>{{ $errors->first('title') }}</strong>
          </span>
      @endif
      <p><input type="text" name="title" placeholder="Titre de l'article" value="{{ $article->title }}"></p>
      @if ($errors->has('body'))
          <span class="help-block">
              <strong>{{ $errors->first('body') }}</strong>
          </span>
      @endif
      <textarea name="body" id="article-body">{!! $article->body !!}</textarea>
      <p class="cover-title">Couverture (optionnel)</p>
      <p>
        <input type="file" id="cover" name="cover">
      </p>
      <p class="cover-title">
          Enregistrer le brouillon
      </p>
      <div class="input-radio">
        <p><label for="yes">Oui</label><input id="yes" type="radio" name="visible" value="0"></p>
        <p><label for="no">Non</label><input id="no" checked type="radio" name="visible" value="1"></p>
      </div>


      <p><input type="submit" value="Valider"></p>
    </form>


</div>


<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
        CKEDITOR.replace( 'article-body', {
          height: 300,

			// Configure your file manager integration. This example uses CKFinder 3 for PHP.
    			filebrowserBrowseUrl: "{{ asset('kcfinder/browse.php?opener=ckeditor&type=files') }}",
    			filebrowserImageBrowseUrl: "{{ asset('kcfinder/browse.php?opener=ckeditor&type=images') }}",
    			filebrowserUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=files') }}",
    			filebrowserImageUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=images') }}",
          filebrowserFlashUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=flash') }}"



        });
</script>
@endsection
