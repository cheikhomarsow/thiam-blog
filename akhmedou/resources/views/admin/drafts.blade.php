@extends('admin.dashboard')
@section('content-bis')
<div class="users-box">
    <h1>Brouillons</h1>
    @if(session('response'))
        <div class="success-block">
            {{ session('response') }}
        </div>
    @endif
    <div class="tbl-header">
        <table cellpadding="0" cellspacing="0" border="0">
            <thead>
                <tr>
                  <th>Titre</th>
                  <th class="status">Editer</th>
                  <th class="remove status">Supprimer</th>
                </tr>
            </thead>
        </table>
      </div>
      <div class="tbl-content">
        <table cellpadding="0" cellspacing="0" border="0">
            <tbody>

                @foreach($articles as $article)
                    <tr>
                      <td>{{ $article->title }}</td>
                      <td class="status">
                        <a href="{{ route('admin.articles.show.draft', $article->slug) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                      </td>
                      <td class="status">
                        <label for="{{ 'remove_'.$article->id }}">
                          <i class="fa fa-trash" aria-hidden="true"></i>
                        </label>
                        <form method="POST" class="none" action="{{ route('admin.articles.destroy', $article->id) }}">
                        {{ csrf_field() }}
                        <input type="submit" id="{{ 'remove_'.$article->id }}" value="Supprimer">
                        </form>
                      </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
