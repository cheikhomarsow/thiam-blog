@extends('layouts.app')

@section('content')
  <div class="admin-dashboard">
    @include('admin.includes.sidebar')
    <div class="admin-box">
        @yield('content-bis')
    </div>
  </div>
@endsection
