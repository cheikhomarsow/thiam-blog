<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/jpg" href="{{ asset('img/logo.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta property="og:type"          content="website" />
    <meta property="og:African agora"         content="Cheikh Omar SOW" />
    <meta property="og:description"   content="African agora" />
    <meta property="og:image"         content="{{ asset('img/logo.jpg') }}" />
    <title>{{ config('app.name') }}</title>

  	<script src="https://cdn.ckeditor.com/4.8.0/standard-all/ckeditor.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="{{ asset('js/header.js') }}"></script>


    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

</head>
<body>
    <div class="container">

        @include('includes.header')

      <div class="content">
        @yield('content')
      </div>

      @include('includes.footer')

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/covers.js') }}"></script>
    <script type="text/javascript">
        (function($) { // Begin jQuery
            $(function() { // DOM ready
                // If a link has a dropdown, add sub menu toggle.
                $('nav ul li a:not(:only-child)').click(function(e) {
                    $(this).siblings('.nav-dropdown').toggle();
                    // Close one dropdown when selecting another
                    $('.nav-dropdown').not($(this).siblings()).hide();
                    e.stopPropagation();
                });
                // Clicking away from dropdown will remove the dropdown class
                $('html').click(function() {
                    $('.nav-dropdown').hide();
                });
                // Toggle open and close nav styles on click
                $('#nav-toggle').click(function() {
                    $('nav ul').slideToggle();
                });
                // Hamburger to X toggle
                $('#nav-toggle').on('click', function() {
                    this.classList.toggle('active');
                });
            }); // end DOM ready
        })(jQuery); // end jQuery
    </script>

    <script>
        $(function(){
              var shrinkHeader = 1;
              $(window).scroll(function() {
              var scroll = getCurrentScroll();
                if ( scroll >= shrinkHeader ) {
                     $('.navigation').addClass('shrink');
                  }
                  else {
                      $('.navigation').removeClass('shrink');
                  }
              });
              function getCurrentScroll() {
              return window.pageYOffset || document.documentElement.scrollTop;
              }
        });
    </script>

</body>
</html>
