@extends('layouts.app')

@section('content')
<div class="contact-page">
    <div class="form-box">
      @if(session('response'))
          <div class="success-block">
              {{ session('response') }}
          </div>
      @endif
        <div class="contact">Me contacter</div>
        <form method="POST" action="{{ route('postContact') }}">
            {{ csrf_field() }}
            <input type="text" placeholder="Nom complet*" name="name">
            <input type="email" placeholder="Email*" name="email">
            <input type="text" placeholder="Object" name="subject">
            <textarea name="bodyMessage" placeholder="Votre message ici..."></textarea>
            <p><input type="submit" value="Envoyer"></p>
        </form>
    </div>
</div>
@endsection
