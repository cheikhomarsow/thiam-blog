@extends('layouts.app')

@section('content')
<div class="home-page">
    <div class="profile-box">
        <div class="informations">Mes informations personnelles</div>

        <div class="user-form-box">
          @if(session('response'))
              <div class="success-block">
                  {{ session('response') }}
              </div>
          @endif
          <form class="form-horizontal" method="POST" action="{{ route('admin.profile.updtate') }}">
                {{ csrf_field() }}

                <div class="box box-un">
                    <input id="email" type="text" class="form-control" value="{{ $user->email}}" name="email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    <input id="firstname" type="text" class="form-control" value="{{ $user->firstname}}" name="firstname">
                    @if ($errors->has('firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                    <input id="lastname" type="text" class="form-control" value="{{ $user->lastname}}" name="lastname">
                    @if ($errors->has('lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif



                    <input id="current_password" type="password" placeholder="Actuel mot de passe" name="current_password">
                    <p class="carrefull">Saisissez votre mot de passe actuel pour changer votre Adresse de courriel ou votre Mot de passe.</p>

                    @if ($errors->has('current_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('current_password') }}</strong>
                        </span>
                    @endif
                  </div>


                <div class="box box-deux">
                  <input id="password" type="password" placeholder="Nouveau mot de passe" class="form-control" name="password">

                  @if ($errors->has('password'))
                      <span class="help-block">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif

                  <input id="password-confirm" type="password" placeholder="Confirmer le nouveau mot de passe" class="form-control" name="password_confirmation">
                </div>
                <p><input type="submit" value="Enregistrer">

            </form>
        </div>
    </div>
</div>
@endsection
