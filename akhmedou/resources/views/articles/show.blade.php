@extends('layouts.app')

@section('content')
<div class="show-article-page">
    <div class="box">
        <div class="show-article-page-box">
          @if($article->first())
            <div class="article">
                <div class="title">
                    {{ $article->title }}
                </div>
                <div class="extras">
                  <span>{{ $article->user->first()->firstname }} {{ $article->user->first()->lastname }} - {{ $article->created_at->diffForHumans() }}</span>
                  <span><i class="fa fa-comments" style="color:#5E5E5E" aria-hidden="true"></i> {{ $article->comments()->count() }}</span>
                  <span><i class="fa fa-eye" style="color:#5E5E5E" aria-hidden="true"></i> {{ $article->view_counter }}</span>
                </div>

                @if($article->cover !== "http://africanagora.com/public/uploads/covers/default.jpg")
                  <div class="img">
                      <img src="{{ $article->cover }}" alt='cover'/>
                  </div>
                @endif

                <div class="body">
                    {!! $article->body !!}
                </div>
                <div class="icons-div">
                    <div><a href="{{ Share::load('http://www.africanagora.com/article/'.$article->slug , $article->title)->facebook() }}" title="Partagez sur Facebook"><i class="fa fa-facebook" style="color:#4267B2" aria-hidden="true"></i></a></div>
                    <div><a href="{{ Share::load('http://www.africanagora.com/article/'.$article->slug , $article->title)->twitter() }}" title="Partagez sur Twitter"><i class="fa fa-twitter" style="color:#4AB3F4" aria-hidden="true"></i></a></div>
                </div>
            </div>
            <div class="comments-box">
                <p class="p"><i class="fa fa-comment-o" aria-hidden="true"></i> Commentaires ({{ $article->comments()->count() }})</p>

                <div class="available-coms">
                    @foreach($article->comments as $comment)
                        <div class="username">{{ $comment->name }}</div>
                        <div class="comment">{{ $comment->comment }}</div>
                    @endforeach
                </div>
                <div class="form-box">
                  <form method="POST" action="{{ route('comments.store', $article->id) }}">
                    {{ csrf_field() }}
                    <input type="text" placeholder="name" name="name">
                    <input type="email" placeholder="email" name="email">
                    <textarea name="comment" placeholder="Votre commentaire ici..."></textarea>
                    <p><input type="submit" value="Laisser un commentaire"></p>
                  </form>
                </div>
            </div>
                @else
                    <div class="article-doesnt-exist">
                        <div>
                    </div>
            @endif
        </div>
        @include('includes.default-sidebar')
    </div>
</div>
@endsection
