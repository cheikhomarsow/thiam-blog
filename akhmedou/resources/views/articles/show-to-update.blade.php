@extends('admin.dashboard')
@section('content-bis')
<div class="articles-form-box">
    <h1>Publier un article</h1>
    @if(session('response'))
        <div class="success-block">
            {{ session('response') }}
        </div>
    @endif
    <form class="reglog-container" method="POST" action="{{ route('admin.articles.post.article.to.update', $article->id) }}"  enctype="multipart/form-data">
      {{ csrf_field() }}
      @if ($errors->has('title'))
          <span class="help-block">
              <strong>{{ $errors->first('title') }}</strong>
          </span>
      @endif
      <p><input type="text" name="title" placeholder="Titre de l'article" value="{{ $article->title }}"></p>
      @if ($errors->has('body'))
          <span class="help-block">
              <strong>{{ $errors->first('body') }}</strong>
          </span>
      @endif
      <textarea name="body" id="article-body">{!! $article->body !!}</textarea>
      <p class="cover-title">Couverture (optionnel)</p>
      <p>
        <input type="file" id="cover" name="cover">
      </p>

      <p><input type="submit" value="Modifier"></p>
    </form>


</div>


<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
        CKEDITOR.replace( 'article-body', {
          height: 300,

			// Configure your file manager integration. This example uses CKFinder 3 for PHP.
    			filebrowserBrowseUrl: "{{ asset('kcfinder/browse.php?opener=ckeditor&type=files') }}",
    			filebrowserImageBrowseUrl: "{{ asset('kcfinder/browse.php?opener=ckeditor&type=images') }}",
    			filebrowserUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=files') }}",
    			filebrowserImageUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=images') }}",
          filebrowserFlashUploadUrl: "{{ asset('kcfinder/upload.php?opener=ckeditor&type=flash') }}"



        });
</script>
@endsection
