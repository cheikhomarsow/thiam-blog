@extends('layouts.app')

@section('content')
<div class="show-article-page">
    <div class="box">
        @if($article)
            <div class="show-article-page-box">
                <div class="article">
                    <div class="title">
                        {{ $article->title }}
                    </div>
                    <div class="extras">
                      <span>{{ $article->user->first()->firstname }} {{ $article->user->first()->lastname }} le {{ $article->created_at->format('d/m/Y') }}</span>
                      <span><i class="fa fa-comments" style="color:#5E5E5E" aria-hidden="true"></i> {{ $article->comments()->count() }}</span>
                      <span><i class="fa fa-eye" style="color:#5E5E5E" aria-hidden="true"></i> {{ $article->view_counter }}</span>
                    </div>

                    @if($article->cover !== "http://127.0.0.1:8000/uploads/covers/default.jpg")
                      <div class="img">
                          <img src="{{ $article->cover }}" alt='cover'/>
                      </div>
                    @endif

                    <div class="body">
                        {!! $article->body !!}
                    </div>
                </div>
                <div class="check">
                  <form method="POST" action="{{ route('admin.articles.check', $article->id) }}">
                      {{ csrf_field() }}
                      <input type="submit" id="{{ 'check_'.$article->id }}" value="Valider">
                  </form>
                </div>
            </div>
            @else
              <div class="article-not-found">Article not found</div>
            @endif
        @include('includes.default-sidebar')
    </div>
</div>
@endsection
