@extends('layouts.app')

@section('content')
<div class="home-page">
    <div class="home-page-box">
        <div class="articles-resumes">
            <div class="resumes">
                @foreach($articles as $article)
                    <div class="recents-articles">
                        <div class="img">
                            <a href="{{ route('article.read', $article->slug) }}"><img src="{{ $article->cover }}" alt="cover"/></a>
                        </div>
                        <div class="content-recents-articles">
                            <div class="title-bis"><a href="{{ route('article.read', $article->slug) }}">{{ $article->title }}</a></div>
                            <div class="body-bis"><p>{!! \Illuminate\Support\Str::words($article->body, 50,' (....)')  !!}</p></div>
                            <div class="author-date">{{ $article->user->first()->firstname }} {{ $article->user->first()->lastname }} le {{ $article->created_at->format('d/m/Y') }}</div>
                            <div class="read-more"><a href="{{ route('article.read', $article->slug) }}">Lire la suite...</a></div>
                        </div>
                    </div>
                    <div class="hr"></div>
                @endforeach
                {{ $articles->links() }}

            </div>
        </div>
        @include('includes.default-sidebar')
  </div>
</div>


@endsection
