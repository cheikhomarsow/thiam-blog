@extends('layouts.app')

@section('content')
<div class="about-page">
    <div class="about">
      <div class="img-about"><img src="{{asset('uploads/covers/default.jpg')}}" alt="default"/></div>
      <div class="about-content">
        <p>Africanagora est né d’une initiative d’un jeune étudiant en droit afin de remédier à un manque cruel de lieux de réflexions.
          Ce blog  se propose ainsi d’être une tribune idéale pour  tout individu qui veut partager ses  idées telles qu’elles soient.</p>
          <p>Nous vous invitons à partager vos articles en <a href="{{ route('getContact') }}">cliquant ici</a>.</p>
        </div>
    </div>
</div>
@endsection
