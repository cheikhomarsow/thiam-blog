<header id="header-desktop">
  <div class="header-container">
    <div class="brand"><a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}"/></a></a></div>
    <nav>
      <ul>
          <li><a href="{{route('home')}}">Accueil</a></li>
          <li><a href="{{route('article.all')}}">Articles</a></li>
          <li><a href="{{route('getContact')}}">Contact</a></li>
          <li><a href="{{route('about')}}">A propos</a></li>
            @guest
              <li><a href="{{ route('login') }}">Espace membre</a></li>
              @else
              <li><a href="#" class="item">{{ Auth::user()->firstname}} <i class="fa fa-user" aria-hidden="true"></i></i></a>
                  <span class="accent"></span>
                  <ul class="drop-down">
                    @if(Auth::user()->supadmin == 1)
                        <li>
                            <a href="{{ route('admin.index') }}">Administration</a>
                        </li>
                    @endif
                    @if(Auth::user()->admin == 1)
                        <li>
                            <a href="{{ route('admin.articles.create') }}">Administration</a>
                        </li>
                    @endif
                    <li>
                        <a href="{{ route('admin.profile') }}">Mon compte</a>
                    </li>
                    <li>
                      <a href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                          Déconnexion
                      </a>

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                    </li>
                  </ul>
              </li>
            @endguest
            <li><i class="fa fa-search" id="search" aria-hidden="true"></i><i class="fa fa-search-minus none" aria-hidden="true"></i></li>
      </ul>
    </nav>
    <div class="none flexsearch-box">
        <form class="flexsearch" action="{{ route('search') }}" method="post">
          {{ csrf_field() }}
            <div class="flexsearch--input-wrapper">
                <input name="query" class="flexsearch--input" type="search" placeholder="Rechercher sur le site...">
            </div>
            <input class="flexsearch--submit" type="submit" value="RECHERCHER"/>
        </form>
    </div>
  </div>
</header>

<div id="header-mobile">
	<div class="mainContainer">
		<header>
			<a href="{{ route('home') }}" class="logo"><img src="{{ asset('img/logo.png') }}"/></a>
			<a href="#" class="menuBtn">
				<span class="lines"></span>
			</a>
			<nav class="mainMenu">
				<ul>
          <li><a href="{{route('home')}}">Accueil</a></li>
          <li><a href="{{route('article.all')}}">Articles</a></li>
          <li><a href="{{route('getContact')}}">Contact</a></li>
          <li><a href="{{route('about')}}">A propos</a></li>

            @guest
              <li><a href="{{ route('login') }}">Espace membre</a></li>
              @else
              @if(Auth::user()->supadmin == 1)
                  <li>
                      <a href="{{ route('admin.index') }}">Administration</a>
                  </li>
              @endif
              @if(Auth::user()->admin == 1)
                  <li>
                      <a href="{{ route('admin.articles.create') }}">Administration</a>
                  </li>
              @endif
              <li>
                  <a href="{{ route('admin.profile') }}">Mon compte</a>
              </li>
              <li>
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Déconnexion
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
            @endguest
				</ul>
			</nav>
		</header>
		<div class="container">
		</div>
		<footer>
		</footer>
	</div>
</div>
