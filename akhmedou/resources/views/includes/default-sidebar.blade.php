<div class="article-resume-sidebar">
  <div class="read-too">A lire aussi</div>
  <div class="other-articles">
      @foreach($more_articles as $article)
          <div class="article">
              <div class="img">
                  <a href="{{ route('article.read', $article->slug) }}"><img src="{{ $article->cover }}" alt="img"/></a>
              </div>
              <div class="article-title">
                  <a href="{{ route('article.read', $article->slug) }}">{!! \Illuminate\Support\Str::words($article->title, 20,' ....'.'</em></strong></a></h1></h2></h3></h4></h5></h6>')  !!}</a>
              </div>
              <div class="article-extras">
                  {{ $article->user->first()->firstname }} {{ $article->user->first()->lastname }} - {{ $article->created_at->diffForHumans() }}
              </div>
          </div>
      @endforeach
  </div>
</div>
