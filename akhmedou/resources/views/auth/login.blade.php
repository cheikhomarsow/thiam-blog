@extends('layouts.app')

@section('content')


<div class="reglog">

  <div class="reglog-box">
    <div class="reglog-triangle"></div>

    <h2 class="reglog-header">Connexion</h2>

    <form class="reglog-container" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}
      <p><input type="email" placeholder="Email" name="email" value="{{ old('email') }}"></p>
      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      <p><input type="password" name="password" placeholder="Mot de passe"></p>
      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
      <p><input type="submit" value="Se connecter"></p>
    </form>
    <div class="reglog-other">
      <!--<p><a href="{{ route('password.request') }}">Mot de passe oublié ?</a></p>-->
      <p><a href="{{ route('register') }}">Créer un compte</a></p>
    </div>
  </div>
</div>
@endsection
