@extends('layouts.app')

@section('content')
<div class="reglog">
  <div class="reglog-box">
    <form class="reglog-container" method="POST" action="{{ route('password.request') }}">
      {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <p><input type="email" name="email" placeholder="Email" value="{{ $email or old('email') }}"></p>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <p><input type="password" placeholder="Mot de passe" name="password"></p>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <p><input placeholder="Confirmer le mot de passe" type="password" name="password_confirmation"></p>
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
        <p><input type="submit" value="Reset Password"></p>

    </form>
  </div>
</div>



@endsection
