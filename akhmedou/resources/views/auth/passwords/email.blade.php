@extends('layouts.app')

@section('content')
<div class="reglog">
  <div class="reglog-box">
    @if (session('status'))
         <div class="success-block">
             {{ session('status') }}
         </div>
     @endif
    <form class="reglog-container" method="POST" action="{{ route('password.email') }}">
      {{ csrf_field() }}
        <p><input type="email" name="email" placeholder="Email" value="{{ $email or old('email') }}"></p>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
        <p><input type="submit" value="Send Password Reset Link"></p>
    </form>
  </div>
</div>
@endsection
