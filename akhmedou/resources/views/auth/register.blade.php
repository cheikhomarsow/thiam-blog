@extends('layouts.app')

@section('content')

<div class="reglog">

  <div class="reglog-box">
    <div class="reglog-triangle"></div>

    <h2 class="reglog-header">Inscription</h2>

    <form class="reglog-container" method="POST" action="{{ route('register') }}">
      {{ csrf_field() }}
      <p><input type="text"  name="firstname" value="{{ old('firstname') }}" placeholder="Prénom"></p>
      @if ($errors->has('firstname'))
          <span class="help-block">
              <strong>{{ $errors->first('firstname') }}</strong>
          </span>
      @endif
      <p><input type="text"  name="lastname" value="{{ old('lastname') }}" placeholder="Nom"></p>
      @if ($errors->has('lastname'))
          <span class="help-block">
              <strong>{{ $errors->first('lastname') }}</strong>
          </span>
      @endif
      <p><input type="email" placeholder="Email" name="email" value="{{ old('email') }}"></p>
      @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
      <p><input type="password" name="password" placeholder="Mot de passe"></p>
      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
      <p><input type="password" name="password_confirmation" placeholder="Confirmer le mot de passe"><p>

        <div class="g-recaptcha" data-sitekey="6LffCkAUAAAAAFVu6PuY1mPMftWMHGMweHk18csd"></div>

      <p><input type="submit" value="S'inscrire"></p>
    </form>
  </div>
</div>
@endsection
