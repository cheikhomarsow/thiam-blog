@extends('layouts.app')

@section('content')
<div class="home-page">
    <div class="home-page-box">
        <div class="articles-resumes">
            @if($most_popular)
              <div class="top-week">
                    <div class="img">
                        <a href="{{ route('article.read', $most_popular->slug) }}"><img src="{{ $most_popular->cover }}" alt="img"/></a>
                    </div>
                    <div class="content-article">
                        <div class="title"><a href="{{ route('article.read', $most_popular->slug) }}">{{ $most_popular->title }}</a></div>
                        <div class="body">{!! \Illuminate\Support\Str::words($most_popular->body, 50,' (....)'.'</em></strong></a></h1></h2></h3></h4></h5></h6>')  !!}</div>
                        <div class="others">
                          <div class="author-date">{{ $most_popular->user->first()->firstname }} {{ $most_popular->user->first()->lastname }} - {{ $most_popular->created_at->diffForHumans() }}</div>
                          <div class="stats">
                            <span><i class="fa fa-comments" style="color:#5E5E5E" aria-hidden="true"></i> {{ $most_popular->comments()->count() }}</span>
                            <span><i class="fa fa-eye" style="color:#5E5E5E" aria-hidden="true"></i> {{ $most_popular->view_counter }}</span>
                          </div>
                          <div class="share-icons">

                            <div><a href="{{ Share::load('http://www.africanagora.com/article/'.$most_popular->slug , $most_popular->title)->facebook() }}"><i class="fa fa-facebook" style="color:#4267B2" aria-hidden="true"></i></a></div>
                            <div><a href="{{ Share::load('http://www.africanagora.com/article/'.$most_popular->slug , $most_popular->title)->twitter() }}"><i class="fa fa-twitter" style="color:#4AB3F4" aria-hidden="true"></i></a></div>
                          </div>
                        </div>
                    </div>
              </div>
            @endif

            <div class="resumes">
                @foreach($articles as $article)
                    <div class="recents-articles">
                        <div class="img">
                            <a href="{{ route('article.read', $article->slug) }}"><img src="{{ $article->cover }}" alt="cover"/></a>
                        </div>
                        <div class="content-recents-articles">
                            <div class="title-bis"><a href="{{ route('article.read', $article->slug) }}">{{ $article->title }}</a></div>
                            <div class="body-bis"><p>{!! \Illuminate\Support\Str::words($article->body, 50,' (....)'.'</em></strong></a></h1></h2></h3></h4></h5></h6>')  !!}</p></div>
                            <div class="author-date">{{ $article->user->first()->firstname }} {{ $article->user->first()->lastname }} {{ $article->created_at->diffForHumans() }}</div>
                            <div class="read-more"><a href="{{ route('article.read', $article->slug) }}">Lire la suite...</a></div>
                        </div>
                    </div>
                    <div class="hr"></div>
                @endforeach

            </div>
        </div>
        @include('includes.default-sidebar')
  </div>
</div>


@endsection
