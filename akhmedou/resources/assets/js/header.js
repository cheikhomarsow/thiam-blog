$(function(){
  var header = $("#header-desktop"),
      yOffset = 0,
      triggerPoint = 150;
  $(window).scroll(function(){
    yOffset = $(window).scrollTop();

    if(yOffset >= triggerPoint){
      header.addClass("minimized");
    }else{
      header.removeClass("minimized");
    }

  });

  $('#search').click(function() {
      $('.flexsearch-box').removeClass('none');
      $('.fa-search-minus').removeClass('none');
      $('#search').addClass('none')
	});
  $('.fa-search-minus').click(function() {
    $('.flexsearch-box').addClass('none');
    $('.fa-search-minus').addClass('none');
    $('#search').removeClass('none')

	});



  $("li").mouseover(function(){
   $(this).find(".drop-down").slideDown(300);
   $(this).find(".accent").addClass("animate");
   $(this).find(".item").css("color","#246188");
  }).mouseleave(function(){
    $(this).find(".drop-down").slideUp(300);
     $(this).find(".accent").removeClass("animate");
  });

  $(document).ready(function(){
	// menu click event
	$('.menuBtn').click(function() {
    $("#header-mobile").css("height", "560px");
    $(".content").css("display","none");
    $('footer').css("display","none");
		$(this).toggleClass('act');
			if($(this).hasClass('act')) {
				$('.mainMenu').addClass('act');

			}
			else {
				$('.mainMenu').removeClass('act');
        $(".content").css("display","block");
        $('footer').css("display","block");
        $("#header-mobile").css("height", "50px");
			}
	});
});
});
