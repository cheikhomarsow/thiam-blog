<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Article;
use App\Comment;

class Comment extends Model
{
    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public static function destroyCom($id)
    {
         $comment = Comment::find($id);
         return $comment->delete();
    }
}
