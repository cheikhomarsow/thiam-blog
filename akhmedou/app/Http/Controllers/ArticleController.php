<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Comment;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required',
        ]);

        $cover = '';

        if(Input::hasFile('cover')){
            $file = Input::file('cover');

            $extension = $file->getClientOriginalExtension();

            if(!Article::extension_accept($extension)){
              return redirect()
                  ->back()
                  ->with('errorFile','Type fichier non accepté');
            }

            do {
                $filename = Article::getUniqueCoverName(Auth::id()) . strrchr( $file->getClientOriginalName(), '.' );
                if($file->move(public_path(). '/uploads/covers',$filename)){
                    $cover = URL::to('/') . '/uploads/covers/' . $filename;
                }
            } while(file_exists($cover));
        }

        $article = new Article();
        $article->user_id = $user = Auth::id();
        $article->title = $request->input('title');
        $article->slug = str_slug($request->input('title'), '-').'-'.Auth::id().'-'.str_random(5);
        $article->body = $request->input('body');
        $article->visible = $request->input('visible');


        if(Auth::user()->supadmin == 1){
          $article->check = 1;
        }else{

              $username = Auth::user()->firstname . ' ' . Auth::user()->lastname;
              $to    = "akhmedouthiam@gmail.com";
              // adresse MAIL OVH liée à l’hébergement.
              $from  = "postmaster@africanagora.com";
              ini_set("SMTP", "smtp.africanagora.com");   // Pour les hébergements mutualisés Windows de OVH
              // *** Laisser tel quel
              $JOUR  = date("Y-m-d");
              $HEURE = date("H:i");
              $Subject = "African Agora [Article] - $JOUR $HEURE";
               $mail_Data = "";
               $mail_Data .= "<html> \n";
               $mail_Data .= "<head> \n";
               $mail_Data .= "<title> Subject </title> \n";
               $mail_Data .= "</head> \n";
               $mail_Data .= "<body> \n";
              $mail_Data .= "<b>Nouveau article</b> <br> \n";
              $mail_Data .= "<br> \n";
              $mail_Data .= "$username a publié un article, <a href='africanagora.com/admin/articles-to-check'>go check</a> <br> \n";
              $mail_Data .= "</body> \n";
              $mail_Data .= "</HTML> \n";
              $headers  = "MIME-Version: 1.0 \n";
              $headers .= "Content-type: text/html; charset=iso-8859-1 \n";
              $headers .= "From: $from  \n";
              $headers .= "Disposition-Notification-To: $from  \n";
         // Message de Priorité haute

         // -------------------------

               $headers .= "X-Priority: 1  \n";

               $headers .= "X-MSMail-Priority: High \n";
               $CR_Mail = TRUE;

               $CR_Mail = @mail ($to, $Subject, $mail_Data, $headers);

        }
        if($cover !== ''){
            $article->cover = $cover;
        }

        $article->save();

        if(Auth::user()->supadmin == 1){
          return redirect()
              ->back()
              ->with('response','Article publié avec succes');
        }
        return redirect()
            ->back()
            ->with('response','Article en attente de validation');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
          if(Article::articleExist($slug)){
              $data = Article::where([
                  ['visible', '1'],
                  ['check', '1'],
                  ['slug', $slug]
                ])
               ->get()[0];
               $article = Article::find($data->id);
               Article::insertUserIpAdressOrIncrementCounter($article->id);
               Carbon::setLocale('fr');
               $more_articles = Article::readToo();
               return view('articles.show', compact('article', 'more_articles'));
          }else{
              return redirect()->back();
          }


    }

    public function showToCheck($slug)
    {
          $data = Article::where([
              ['visible', '1'],
              ['slug', $slug]
            ])
           ->get()[0];

           $article = Article::find($data->id);

           $more_articles = Article::readToo();





           return view('articles.show-to-check', compact('article', 'more_articles'));

    }


    public function getArticleToUpdate($id)
    {
        $article = Article::find($id);
        return view('articles.show-to-update', compact('article'));
    }


    public function articles()
    {
        $articles = Article::orderBy('created_at', 'desc')
          ->where('visible', '1')
          ->paginate(10);
        return view('admin.articles', compact('articles'));
    }

    public function myArticles()
    {
        $articles = Article::orderBy('created_at', 'desc')
          ->where([
              ['user_id', Auth::id()],
              ['visible', '1']
            ])
          ->paginate(10);
        return view('admin.my-articles', compact('articles'));
    }

    public function allArticles()
    {

        $articles = Article::orderBy('created_at', 'desc')
          ->where([
            ['visible', '1'],
            ['check' , '1']
          ])
          ->paginate(10);

        $more_articles = Article::readToo();

        return view('articles.all-articles', compact('articles', 'more_articles'));
    }

    public function articlesToCheck()
    {
        $articles = Article::orderBy('created_at', 'desc')->where('check', '0')->get();
        return view('admin.articles-to-check', compact('articles'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function updateDraft(Request $request, $slug)
    {
      $this->validate($request,[
          'title' => 'required',
          'body' => 'required',
          'visible' => 'required'
      ]);

      $article = Article::where([
            ['user_id', Auth::id()],
            ['visible', '0'],
            ['slug', $slug]
          ])
         ->get()[0];

      $article->title = $request->input('title');
      $article->body = $request->input('body');
      $article->visible = $request->input('visible');
      $article->created_at = Carbon::now();

      if(Input::hasFile('cover')){
          $file = Input::file('cover');

          $extension = $file->getClientOriginalExtension();

          if(!Article::extension_accept($extension)){
            return redirect()
                ->back()
                ->with('errorFile','Type fichier non accepté');
          }

          do {
              $filename = Article::getUniqueCoverName(Auth::id()) . strrchr( $file->getClientOriginalName(), '.' );
              $file->move(public_path(). '/uploads/covers',$filename);
              $cover = URL::to('/') . '/uploads/covers/' . $filename;
              $path = parse_url($article->cover);
              File::delete(public_path($path['path']));
              $article->cover = $cover;
          } while(file_exists($cover));
      }

      $article->update();

      if($article->visible == 1){
          return redirect('/admin/articles/my-articles');
      }

      return redirect()
          ->back()
          ->with('response','Post modifié avec succes');

    }

    public function postArticleToUpdate(Request $request, $id)
    {
      $this->validate($request,[
          'title' => 'required',
          'body' => 'required',
      ]);

      $article = Article::where([
            ['user_id', Auth::id()],
            ['visible', '1'],
            ['check', '1'],
            ['id', $id]
          ])
         ->get()[0];

      $article->title = $request->input('title');
      $article->body = $request->input('body');

      if(Input::hasFile('cover')){
          $file = Input::file('cover');

          $extension = $file->getClientOriginalExtension();

          if(!Article::extension_accept($extension)){
            return redirect()
                ->back()
                ->with('errorFile','Type fichier non accepté');
          }

          do {
              $filename = Article::getUniqueCoverName(Auth::id()) . strrchr( $file->getClientOriginalName(), '.' );
              $file->move(public_path(). '/uploads/covers',$filename);
              $cover = URL::to('/') . '/uploads/covers/' . $filename;
              $path = parse_url($article->cover);
              File::delete(public_path($path['path']));
              $article->cover = $cover;
          } while(file_exists($cover));
      }

      $article->update();


      return redirect()
          ->back()
          ->with('response','Post modifié avec succes');

    }
     public function destroy($id)
     {
          $article = Article::find($id);

          $article->comments()->delete();

          DB::table('users_ip')->where('article_id', $id)->delete();

          $article->delete();

          return redirect()
            ->back();
     }





     public function check($id)
     {
       $article=Article::find($id);
       $article->check = 1;
       $article->save();
       return redirect()->back();
     }

     public function drafts(){
       $articles = Article::orderBy('created_at', 'desc')
          ->where([
             ['user_id', Auth::id()],
             ['visible', '0']
           ])
          ->get();
       return view('admin.drafts', compact('articles'));
     }

     public function showDraft($slug)
     {
       $article = Article::where([
             ['user_id', Auth::id()],
             ['visible', '0'],
             ['slug', $slug]
           ])
          ->get()[0];

       return view('admin.show-draft', compact('article'));
     }

     public function checkDraft($id)
     {
       $article=Article::find($id);
       $article->visible = 1;
       $article->save();
       return redirect()->back();
     }
}
