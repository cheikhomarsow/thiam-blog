<?php
namespace App\Http\Controllers;

use JavaScript;
use Illuminate\Http\Request;
use App\User;
use App\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;



class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        Carbon::setLocale('fr');

        $articles = Article::where([
            ['visible','1'],
            ['check','1'],
          ])->orderBy('created_at', 'desc')->get();

        $most_popular = Article::mostPopular();

        $more_articles = Article::readToo();



        return View::make('home', compact('articles', 'most_popular', 'more_articles'));

    }
}
