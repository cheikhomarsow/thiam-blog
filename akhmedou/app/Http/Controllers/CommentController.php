<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use Session;

class CommentController extends Controller
{

      public function store(Request $request, $id)
      {
          $this->validate($request, array(
              'name' => 'required|max:255',
              'email' => 'required|email|max:255',
              'comment' => 'required|min:5|max:2000'
          ));

          $article = Article::find($id);

          $comment = new Comment();
          $comment->name = $request->name;
          $comment->email = $request->email;
          $comment->comment = $request->comment;
          $comment->approved = true;
          $comment->article_id = $article->id;

          $comment->save();

          Session::flash('success','Commentaire ajouté');

          return redirect()->route('article.read', [$article->slug]);
      }

      public function availableComments($id)
      {
          $comments = Article::find($id)->comments();

          dd($comments);
      }

}
