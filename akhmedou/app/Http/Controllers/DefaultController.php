<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;
use Session;
use App\Article;

class DefaultController extends Controller
{
    public function getContact()
    {
        return view('contact');
    }

    public function about()
    {
        return view('about');
    }

    public function postContact(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'min:2',
            'object' => 'min:3',
            'body' => 'min:10'
        ]);

        $data = array(
            'email' => $request->email,
            'name' => $request->name,
            'subject' => $request->subject,
            'bodyMessage' => $request->bodyMessage
        );

        $to    = "akhmedouthiam@gmail.com";
        // adresse MAIL OVH liée à l’hébergement.
        $from  = "postmaster@africanagora.com";
        ini_set("SMTP", "smtp.africanagora.com");   // Pour les hébergements mutualisés Windows de OVH
        // *** Laisser tel quel
        $JOUR  = date("Y-m-d");
        $HEURE = date("H:i");
        $Subject = "African Agora [Contact] - $JOUR $HEURE";
         $mail_Data = "";
         $mail_Data .= "<html> \n";
         $mail_Data .= "<head> \n";
         $mail_Data .= "<title> Subject </title> \n";
         $mail_Data .= "</head> \n";
         $mail_Data .= "<body> \n";
        $mail_Data .= "<b>Contact</b> <br> \n";

       $mail_Data .= "<br> \n";

       $mail_Data .= "$request->bodyMessage <br> \n";

       $mail_Data .= "Email: $request->email.<br> \n";

       $mail_Data .= "</body> \n";

       $mail_Data .= "</HTML> \n";



   $headers  = "MIME-Version: 1.0 \n";

   $headers .= "Content-type: text/html; charset=iso-8859-1 \n";

   $headers .= "From: $from  \n";

   $headers .= "Disposition-Notification-To: $from  \n";



   // Message de Priorité haute

   // -------------------------

   $headers .= "X-Priority: 1  \n";

   $headers .= "X-MSMail-Priority: High \n";



   $CR_Mail = TRUE;



   $CR_Mail = @mail ($to, $Subject, $mail_Data, $headers);

   if ($CR_Mail === FALSE)

     {

     Session::flash('response', 'Votre message a été envoyé !');
     return redirect()->back();

     }

  else

     {

       return redirect()->back();


     }

    }

    public function getSearch()
    {
        $q = '';
        $articles = collect([]);
        $more_articles = Article::readToo();


        return view('search-results', compact('q', 'articles', 'more_articles'));
    }

    public function search(Request $request)
    {

        $q = $request->input('query');

        if(trim($q) == "" || strlen($q) < 3){
            return redirect()->back();
        }

        $articles = Article::where([
            ['title', 'like', '%'.$q.'%'],
            ['visible', '1'],
            ['check', '1'],
        ])
        ->orWhere([
          ['body', 'like', '%'.$q.'%'],
          ['visible', '1'],
          ['check', '1'],
        ])
        ->orderBy('created_at', 'desc')->paginate(10);

        $more_articles = Article::readToo();

        return view('search-results', compact('articles', 'more_articles', 'q'));
    }
}
