<?php

namespace App\Http\Controllers;

use App\Rules\UserPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{



    public function getProfile(){
        $user = Auth::user();
        return view('users.profile', compact('user'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
          $currentUser = Auth::user();
          $this->validate($request,[
            'email' => 'required|string|email|max:255|unique:users,id,'.$currentUser->id,
            'firstname' => 'required|string|min:2',
            'lastname' => 'required|string|min:2',
            'current_password' => new UserPassword,
          ]);

          $user = User::find(Auth::id());

          $user->email = $request->input('email');
          $user->firstname = $request->input('firstname');
          $user->lastname = $request->input('lastname');

          if(!is_null($request->input('password'))){
            $this->validate($request,[
              'password' => 'required|string|min:6|different:current_password|confirmed',
            ]);
            $user->password = Hash::make($request->input('password'));
          }

          $user->update();

          return redirect()->back()
             ->with('response', 'Informations modifiées avec success!');
    }


}
