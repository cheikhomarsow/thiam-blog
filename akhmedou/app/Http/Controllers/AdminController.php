<?php

namespace App\Http\Controllers;

use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $users = User::all();

        $supadmin = collect(User::where('supadmin', 1)
               ->get()[0])->toArray();
        $admins = collect(User::where('admin', 1)
               ->orderBy('id','desc')
               ->get())->toArray();

        $users = collect(User::where([
                ['admin', 0],
                ['supadmin', 0]
              ])
               ->orderBy('id','desc')
               ->get())->toArray();

        return view('admin.users',compact('supadmin', 'admins', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

         $user=User::find($id);

         if((int)$user->admin == 1){
            $user->admin=1-(int)$user->admin;

         }else{
            $user->admin = 1 + (int)$user->admin;
         }

         $user->save();
         return redirect()->back();

    }


    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();

         return redirect()
           ->back();
    }

    
}
