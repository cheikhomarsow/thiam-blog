<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use App\Comment;
use Mail;


class Article extends Model
{
      public $timestamps = true;

      protected $fillable = [
          'title', 'body', 'check', 'cover'
      ];

      public function user()
      {
          return $this->belongsTo('App\User');
      }

      public function comments()
      {
          return $this->hasMany('App\Comment');
      }

      public static function destroyArticle($id)
      {
         $article = Article::find($id);
         return $article->delete();
      }

      public static function extension_accept($extension){
      		$extension_autorisees = array('jpg', 'jpeg','png');
      		return in_array($extension , $extension_autorisees);
      }

      public static function getUniqueCoverName($user_id){
          return $user_id . '_' . str_random(10);
      }

      public static function countComments($article)
      {
          return $article->comments()->count();
      }

      public static function getUserIpAdress(){
          return \Request::ip();
      }

      public static function insertUserIpAdressOrIncrementCounter($article_id)
      {
          if(Article::articleAlreadyRead($article_id)){
              $data = DB::table('users_ip')->where([
                  ['ip', Article::getUserIpAdress()],
                  ['article_id',$article_id]
              ])->first();

              $current_time = Carbon::now()->timestamp;

              if(($current_time - (int)($data->insert_at)) >= 259200){
                  DB::table('users_ip')
                      ->where([
                            ['ip', Article::getUserIpAdress()],
                            ['article_id',$article_id],
                        ])
                      ->update(['insert_at' => $current_time]);

                    Article::incrementCounter($article_id, 1);
              }

          }else{
              DB::table('users_ip')->insert(
                  [
                    'ip' => Article::getUserIpAdress(),
                    'article_id' => $article_id,
                    'insert_at' => Carbon::now()->timestamp
                  ]
              );
              Article::incrementCounter($article_id, 1);
          }

      }

      public static function incrementCounter($article_id, $nb)
      {
          $article = Article::find($article_id);
          $article->increment('view_counter',$nb);
      }

      public static function articleAlreadyRead($article_id)
      {
          return (DB::table('users_ip')->where([
              ['ip', Article::getUserIpAdress()],
              ['article_id',$article_id]
          ])->first() !== null);
      }

      public static function articleExist($slug){
          return Article::where('slug',$slug)->first() !== null;
      }

      public static function mostPopular()
      {

          $data = Article::all();
          $data2 = $data->where('view_counter', $data->max('view_counter'))->first();


          if($data2->first()->check == 1 AND $data2->first()->check == 1){
              return $data2;
          }

      }

      public static function readToo()
      {
          return Article::where([
              ['visible','1'],
              ['check','1'],
              ['view_counter', '<', '100']
            ])->limit(7)->get();
      }

      public static function prevent($firstname, $lastname, $email)
      {
          $data = array(
              'email' => $email,
              'firstname' => $firstname,
              'lastname' => $lastname,
          );

          Mail::send('emails.prevent', $data, function($message) use ($data){
              $message->from('africanagorablog@gmail.com');
              $message->to('postmaster@africanagora.com');
              $message->subject('African agora [Article à valider]');
          });

      }

      public function delete()
      {
          $this->comments()->delete();
          return parent::delete();
      }




}
