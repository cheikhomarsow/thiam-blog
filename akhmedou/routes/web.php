<?php

use App\Http\Middleware\supadmin;
use App\Http\Middleware\admin;

Route::get('/', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});



Route::middleware('web')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/article/{slug}', 'ArticleController@show')->name('article.read');
    Route::post('/article/{slug}', 'CommentController@store')->name('comments.store');
    Route::get('/articles', 'ArticleController@allArticles')->name('article.all');
    Route::get('/contact', 'DefaultController@getContact')->name('getContact');
    Route::post('/contact', 'DefaultController@postContact')->name('postContact');
    Route::get('/search', 'DefaultController@getSearch')->name('getSearch');
    Route::post('/search', 'DefaultController@search')->name('search');

    Route::get('/about', 'DefaultController@about')->name('about');

});

Route::middleware('web')->group(function () {
  Route::get('/profile', 'UserController@getProfile')->name('admin.profile');
  Route::post('/profile', 'UserController@update')->name('admin.profile.updtate');
});



Route::prefix('admin')->group(function(){
  Route::middleware('auth')->group(function () {
    Route::middleware(supadmin::class)->group(function () {
        Route::get('/', 'AdminController@index')->name('admin.index');
        Route::get('/users', 'AdminController@index')->name('admin.users');
        Route::post('/users/destroy/{id}', 'AdminController@destroy')->name('admin.users.destroy');
        Route::post('/users/update/{id}', 'AdminController@update')->name('admin.users.update');
        Route::get('/articles', 'ArticleController@articles')->name('admin.articles');
        Route::get('/articles-to-check', 'ArticleController@articlesToCheck')->name('admin.articles.to.check');
        Route::post('/articles/check/{id}', 'ArticleController@check')->name('admin.articles.check');
        Route::get('/article/tocheck/{slug}', 'ArticleController@showToCheck')->name('article.tocheck.read');

    });

    Route::middleware(admin::class)->group(function () {
      Route::prefix('articles')->group(function(){
        Route::get('/create', 'ArticleController@create')->name('admin.articles.create');
        Route::post('/create', 'ArticleController@store')->name('admin.articles.store');

        Route::get('/my-articles', 'ArticleController@myArticles')->name('admin.my-articles');
        Route::post('/destroy/{id}', 'ArticleController@destroy')->name('admin.articles.destroy');
        Route::get('/drafts', 'ArticleController@drafts')->name('admin.articles.drafts');

        Route::get('/drafts/show/{slug}', 'ArticleController@showDraft')->name('admin.articles.show.draft');
        Route::post('/drafts/show/{slug}', 'ArticleController@updateDraft')->name('admin.articles.update.draft');

        Route::get('/update/{id}', 'ArticleController@getArticleToUpdate')->name('admin.articles.get.article.to.update');
        Route::post('/update/{id}', 'ArticleController@postArticleToUpdate')->name('admin.articles.post.article.to.update');

      });
    });
  });
});


//Route::post('/password/reset/{token}', 'Auth\ResetPasswordController@reset');
