<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();
      DB::table('users')->insert([
          [
            'firstname'=>'Akhmedou',
            'lastname' => 'THIAM',
            'email' => 'akhmedouthiam@gmail.com',
            'password' => bcrypt('password1'),
            'avatar' => '',
            'supadmin' => true,
            'admin' => false
          ]
      ]);
    }
}
