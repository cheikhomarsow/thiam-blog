<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;


class CreateUsersIpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_ip', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->integer('article_id')->unsigned();
            $table->string('insert_at');
            $table->timestamps();
            $table->foreign('article_id')->references('id')
               ->on('articles')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_ip');
    }
}
